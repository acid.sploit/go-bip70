package main

import (
	"crypto"
	"crypto/ecdsa"
	"crypto/ed25519"
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha256"
	"crypto/x509"
	"encoding/hex"
	"encoding/json"
	"errors"
	"strconv"
	"time"

	"github.com/gcash/bchd/chaincfg"
	"github.com/gcash/bchd/txscript"
	"github.com/gcash/bchutil"
	_ "github.com/go-sql-driver/mysql"
	"github.com/gogo/protobuf/proto"
	"github.com/rs/zerolog/log"
	payments "gitlab.com/acid.sploit/go-bip70/payments"
)

// Output internal datatype, expected in payment
type Output struct {
	Amount  uint64
	Address string
	Script  []byte

	// Address bchutil.Address
	// Proto   *payments.Output
}

// NewOutput initialize new output
func NewOutput(amount uint64, address string) (*Output, error) {
	addr, err := bchutil.DecodeAddress(address, &chaincfg.MainNetParams)
	if err != nil {
		// log.Error().Msgf("unable to decode address: %s", address)
		// http.Error(w, "internal server error", http.StatusInternalServerError)
		return nil, errors.New("unable to decode address: " + address + " error: " + err.Error())
	}
	script, err := txscript.PayToAddrScript(addr)
	if err != nil {
		// log.Error().Msgf("unable to get p2pkh script address : %s", out.Address.String())
		// http.Error(w, "internal server error", http.StatusInternalServerError)
		return nil, errors.New("unable to get p2pkh script address: " + addr.String() + " error: " + err.Error())
	}

	var output = &Output{
		Amount:  amount,
		Address: addr.EncodeAddress(),
		Script:  script,
	}

	return output, nil

}

func (out *Output) setAmount(amount uint64) error {
	if amount > 0 && amount < 546 {
		return errors.New("amount below dust treshold")
	}
	out.Amount = amount

	return nil
}

// setAddressAndScript decodes bch address from string and derives p2pkh script from address
// func (out *Output) setAddressAndScript(address string) error {
// 	if err := out.setAddress(address); err != nil {
// 		return err
// 	}

// 	if err := out.parseScriptFromAddress(); err != nil {
// 		return err
// 	}

// 	return nil
// }

// func (out *Output) setAddress(address string) error {
// 	addr, err := bchutil.DecodeAddress(address, &chaincfg.MainNetParams)
// 	if err != nil {
// 		// log.Error().Msgf("unable to decode address: %s", address)
// 		// http.Error(w, "internal server error", http.StatusInternalServerError)
// 		return errors.New("unable to decode address: " + address + " error: " + err.Error())
// 	}
// 	out.Address = addr

// 	err = out.parseScriptFromAddress()
// 	if err != nil {
// 		return err
// 	}

// 	return nil
// }

// func (out *Output) parseScriptFromAddress() error {
// 	script, err := txscript.PayToAddrScript(out.Address)
// 	if err != nil {
// 		// log.Error().Msgf("unable to get p2pkh script address : %s", out.Address.String())
// 		// http.Error(w, "internal server error", http.StatusInternalServerError)
// 		return errors.New("unable to get p2pkh script address: " + out.Address.String() + " error: " + err.Error())
// 	}

// 	out.Script = script

// 	return nil
// }

// GetProto returns protobuf output objects
func (out *Output) GetProto() *payments.Output {
	var output = &payments.Output{
		Amount: &out.Amount,
		Script: out.Script,
	}

	return output
}

func (out *Output) String() string {
	str := "address=\"" + out.Address + "\", amount=" + strconv.FormatUint(out.Amount, 10) + "\""
	return str
}

// PaymentRequest internal datatype
type PaymentRequest struct {
	ID                  string
	PDVersion           uint32
	PKIType             string
	PKIData             []byte
	Network             string
	Outputs             []*Output
	Time                time.Time
	Expires             time.Time
	Memo                string
	PaymentURL          string
	PaymentRequestProto *payments.PaymentRequest
}

func (pr *PaymentRequest) addOutput(amount uint64, address string) error {
	output, err := NewOutput(amount, address)
	if err != nil {
		return err
	}

	pr.Outputs = append(pr.Outputs, output)

	return nil
}

func (pr *PaymentRequest) getOutputsProto() []*payments.Output {
	var outputs []*payments.Output
	for _, output := range pr.Outputs {
		outputs = append(outputs, output.GetProto())
	}

	return outputs
}

func (pr *PaymentRequest) getPaymentDetailsProto() *payments.PaymentDetails {
	time := uint64(pr.Time.Unix())
	expire := uint64(pr.Expires.Unix())

	var pd = &payments.PaymentDetails{
		Network:    &pr.Network,
		Outputs:    pr.getOutputsProto(),
		Time:       &time,
		Expires:    &expire,
		Memo:       &pr.Memo,
		PaymentUrl: &pr.PaymentURL,
	}

	return pd
}

func (pr *PaymentRequest) marshalPaymentDetails() ([]byte, error) {
	spd, err := proto.Marshal(pr.getPaymentDetailsProto())
	if err != nil {
		log.Error().Msgf("unable to marshal request to proto: %s", err)
		return nil, err
	}

	return spd, nil
}

func (pr *PaymentRequest) setPKIType(pkiType string) {
	// TODO: handle input for select pki types none, "x509+sha256", "x509+sha1"
	pr.PKIType = pkiType
}

func (pr *PaymentRequest) setPKIData(cert *x509.Certificate, chain *x509.Certificate) error {
	var certs [][]byte
	certs = append(certs, cert.Raw)
	certs = append(certs, chain.Raw)

	X509Certs := &payments.X509Certificates{
		Certificate: certs,
	}

	serializedX509Certs, err := proto.Marshal(X509Certs)
	if err != nil {
		// log.Error().Msgf("%s unable to marshal x509 certs to proto: %s", id, err)
		// http.Error(w, "internal server error", http.StatusInternalServerError)
		return errors.New("unable to marshal x509 certs to protobuf: " + err.Error())
	}

	pr.PKIData = serializedX509Certs

	return nil
}

func (pr *PaymentRequest) marshalPaymentRequest(privKey interface{}) ([]byte, error) {
	spd, err := pr.marshalPaymentDetails()
	if err != nil {
		return nil, err
	}

	var prProto = &payments.PaymentRequest{
		PaymentDetailsVersion:    &pr.PDVersion,
		PkiType:                  &pr.PKIType,
		PkiData:                  pr.PKIData,
		SerializedPaymentDetails: spd,
		Signature:                []byte{},
	}

	resp, err := proto.Marshal(prProto)
	if err != nil {
		return nil, errors.New("unable to marshal request to proto: " + err.Error())
	}
	respHash := sha256.Sum256(resp)
	hash := respHash[:]

	var signature []byte
	switch privKey.(type) {
	case *rsa.PrivateKey:
		signature, err := rsa.SignPKCS1v15(rand.Reader, privKey.(*rsa.PrivateKey), crypto.SHA256, hash)
		if err != nil {
			log.Error().Msgf("failed to sign payment request: %s", err)
			return nil, errors.New("failed to sign payment request: " + err.Error())
		}
		signatureHash := sha256.Sum256(signature)
		log.Debug().Str("signature-hash", hex.EncodeToString(signatureHash[:])).Msgf("%s signature", pr.ID)
	case *ecdsa.PrivateKey:
		return nil, errors.New("privkey type (*ecdsa.PrivateKey) not implemented")
	case ed25519.PrivateKey:
		return nil, errors.New("privkey type (ed25519.PrivateKey) not implemented")
	default:
		return nil, errors.New("privkey did not match know private key type")
	}

	prProto.Signature = signature

	pr.PaymentRequestProto = prProto

	resp, err = proto.Marshal(prProto)
	if err != nil {
		return nil, errors.New("unable to marshal request to proto: " + err.Error())
	}

	return resp, nil
}

func (pr *PaymentRequest) setPDVersion(version uint32) error {
	pr.PDVersion = version

	return nil
}

func (pr *PaymentRequest) redisSet() error {
	b, err := json.Marshal(pr)
	if err != nil {
		return err
	}
	// fmt.Printf("%x", b)
	err = s.rd.Set(pr.ID, string(b), 0).Err()
	if err != nil {
		return err
	}

	return nil
}

func (pr *PaymentRequest) redisGet() error {

	if pr.ID == "" {
		return errors.New("pr.ID not set")
	}

	val, err := s.rd.Get(pr.ID).Result()
	if err != nil {
		return err
	}

	b := []byte(val)

	err = json.Unmarshal(b, pr)
	if err != nil {
		return err
	}

	return nil
}
