package main

import (
	"context"

	pb "gitlab.com/acid.sploit/go-bip70/pb"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
)

func initGRPC() (*grpc.ClientConn, error) {
	conn, err := grpc.Dial(s.cfg.grpcURI, grpc.WithTransportCredentials(credentials.NewClientTLSFromCert(nil, "")), grpc.WithDefaultCallOptions(grpc.MaxCallRecvMsgSize(33554432)))
	if err != nil {
		//fmt.Println(err)
		return nil, err
	}

	return conn, nil
}

func getUnspentOutput(hash []byte, index uint32, includeMempool bool) (*pb.GetUnspentOutputResponse, error) {
	c := pb.NewBchrpcClient(s.bchrpc)

	getUnspentOutputResp, err := c.GetUnspentOutput(context.Background(), &pb.GetUnspentOutputRequest{
		Hash:           hash,
		Index:          index,
		IncludeMempool: includeMempool,
	})

	if err != nil {
		return nil, err
	}

	return getUnspentOutputResp, nil
}

func getBlockchainInfo() (*pb.GetBlockchainInfoResponse, error) {
	c := pb.NewBchrpcClient(s.bchrpc)

	getBlockchainInfoResp, err := c.GetBlockchainInfo(context.Background(), &pb.GetBlockchainInfoRequest{})
	if err != nil {
		return nil, err
	}

	return getBlockchainInfoResp, nil
}

func submitTransaction(transaction []byte) (*pb.SubmitTransactionResponse, error) {
	c := pb.NewBchrpcClient(s.bchrpc)

	submitTransactionResp, err := c.SubmitTransaction(context.Background(), &pb.SubmitTransactionRequest{
		Transaction: transaction,
	})
	if err != nil {
		return nil, err
	}

	return submitTransactionResp, nil
}
