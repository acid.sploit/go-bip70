package main

import (
	"crypto/rsa"
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/gogo/protobuf/proto"
	"github.com/google/uuid"

	"github.com/rs/zerolog/log"

	payments "gitlab.com/acid.sploit/go-bip70/payments"
)

type oput struct {
	address string
	amount  uint64
}

func paymentRequestHandler(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()

	// Generate unique id for new payment request
	uuid := strings.Split(uuid.New().String(), "-")
	id := uuid[len(uuid)-1]
	log.Info().Str("uid", id).Msgf("%s NEW paymentrequest", id)

	// Parse request uri
	reqAddress := r.URL.Query().Get("addr")
	reqAmount, err := strconv.ParseUint(r.URL.Query().Get("amount"), 10, 64)
	if err != nil {
		log.Fatal().Msgf("%s invalid amount - error converting amount to uint64, %s", id, err)
	}

	if reqAddress == "" {
		log.Error().Msgf("%s payment address can not be empty", id)
		http.Error(w, "internal server error - payment address can not be empty", http.StatusInternalServerError)
		return
	}

	log.Info().Str("addr", reqAddress).Uint64("amount", reqAmount).Msgf("%s request params", id)

	newAddr, err := s.wallet.getNewAddress()
	if err != nil {
		log.Error().Msgf("%s error deriving new payment address: %s", id, err.Error())
		http.Error(w, "internal server error - payment address can not be empty", http.StatusInternalServerError)
		return
	}

	var cashaddrs = []*oput{
		&oput{
			address: newAddr.String(),
			amount:  uint64(12345),
		},
		&oput{
			address: reqAddress,
			amount:  reqAmount,
		},
	}

	log.Info().Str("addr", newAddr.String()).Uint64("amount", uint64(12345)).Msgf("%s pr fee", id)

	// headers := r.Header
	// fmt.Println(headers)

	// Check BIP70 headers
	acceptHeader := r.Header.Get("Accept")
	log.Debug().Str("Accept", acceptHeader).Msgf("%s accept header", id)
	if acceptHeader != "application/bitcoincash-paymentrequest" {
		http.Error(w, "invalid accept header", http.StatusNotAcceptable)
		return
	}

	url := "https://local.devzero.be:4443/payment/?i=" + id
	memo := "Payment to " + url

	var paymentRequest = &PaymentRequest{
		ID:         id,
		Network:    "main",
		Time:       time.Now(),
		Expires:    time.Now().Add(3600 * time.Second),
		PaymentURL: url,
		Memo:       memo,
		Outputs:    []*Output{},
	}

	for _, out := range cashaddrs {
		if err := paymentRequest.addOutput(out.amount, out.address); err != nil {
			log.Error().Msgf("%s error adding output to pr: %s", id, err.Error())
			http.Error(w, "internal server error", http.StatusInternalServerError)
			return
		}
	}

	//
	// TODO: check pritvkey type and act accordingly
	//
	pk := s.privkey.(*rsa.PrivateKey)
	certHash := sha256.Sum256(s.cert.Raw)
	chainHash := sha256.Sum256(s.chain.Raw)
	log.Debug().Str("certificate-hash", hex.EncodeToString(certHash[:])).Msgf("%s certificate", id)
	log.Debug().Str("chain-hash", hex.EncodeToString(chainHash[:])).Msgf("%s certificate chain", id)

	paymentRequest.setPKIType("x509+sha256")
	if err := paymentRequest.setPKIData(s.cert, s.chain); err != nil {
		log.Error().Msgf(err.Error())
		http.Error(w, "internal server error", http.StatusInternalServerError)
		return
	}
	paymentRequest.setPDVersion(uint32(1))

	resp, err := paymentRequest.marshalPaymentRequest(pk)
	if err != nil {
		log.Error().Msgf("%s could not marshal payment request protobuf: %s", id, err.Error())
		http.Error(w, "internal server error", http.StatusInternalServerError)
		return
	}

	// Write payment request protobuf message to disk
	err = ioutil.WriteFile("pr-files/"+id+".proto", resp, 0640)
	if err != nil {
		log.Error().Msgf("%s could not save file to disk: %s", id, err)
		http.Error(w, "internal server error", http.StatusInternalServerError)
		return
	}
	log.Info().Msgf("%s payment request saved to disk", id)

	// Save pr to mysql
	err = savePaymentRequest(id, paymentRequest.PaymentRequestProto, paymentRequest.getPaymentDetailsProto(), paymentRequest.getOutputsProto())
	if err != nil {
		log.Panic().Msgf("%s could not insert pr in database: %s", id, err)
		http.Error(w, "internal server error", http.StatusInternalServerError)
		return
	}
	log.Info().Msgf("%s payment request saved to mysql database", id)

	// Save pr to redis
	b, err := json.Marshal(&paymentRequest)
	if err != nil {
		log.Panic().Msgf("%s could not save pr in redis db: %s", id, err)
		http.Error(w, "internal server error", http.StatusInternalServerError)
		return
	}
	err = s.rd.Set(id, string(b), 0).Err()
	if err != nil {
		log.Panic().Msgf("%s could not save pr in redis db: %s", id, err)
		http.Error(w, "internal server error", http.StatusInternalServerError)
		return
	}
	log.Info().Msgf("%s payment request saved to redis database", id)

	w.Header().Add("Content-Type", "application/bitcoincash-paymentrequest")
	w.Header().Set("Content-Transfer-Encoding", "binary")
	w.Write(resp)

	log.Info().Msgf("%s payment request sent to customer wallet", id)
}

func paymentHandler(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()
	id := r.URL.Query().Get("i")
	log.Info().Msgf("%s NEW payment", id)

	// headers := r.Header
	// fmt.Println(headers)

	// Check BIP70 headers
	contentType := r.Header.Get("Content-Type")
	log.Debug().Str("ContentType", contentType).Msgf("%s content type", id)
	if contentType != "application/bitcoincash-payment" {
		http.Error(w, "invalid content type", http.StatusUnsupportedMediaType)
		return
	}
	acceptHeader := r.Header.Get("Accept")
	log.Debug().Str("Accept", acceptHeader).Msgf("%s accept header", id)
	if acceptHeader != "application/bitcoincash-paymentack" {
		http.Error(w, "invalid content type", http.StatusNotAcceptable)
		return
	}

	var pr = &PaymentRequest{
		ID: id,
	}
	if err := pr.redisGet(); err != nil {
		log.Error().Msgf("%s unable to get payment request from redis db: %s", id, err.Error())
		http.Error(w, "internal server error", http.StatusInternalServerError)
		return
	}

	if r.Method == "POST" {
		log.Debug().Msgf("%s POST request", id)
		// Read the Payment information
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			log.Error().Msgf("%s unable to read request body: %s", id, err.Error())
			http.Error(w, "internal server error", http.StatusInternalServerError)
			return
		}
		payment := &payments.Payment{}
		err = proto.Unmarshal(body, payment)
		if err != nil {
			log.Error().Msgf("%s unable to unmarshal payment: %s", id, err.Error())
			http.Error(w, "internal server error", http.StatusInternalServerError)
			return
		}

		log.Info().Str("memo", payment.GetMemo()).Msgf("%s Payment received", id)

		err = validatePayment(id, payment, pr)
		if err != nil {
			log.Error().Msgf("%s invalid payment: %s", id, err.Error())
			http.Error(w, "internal server error", http.StatusInternalServerError)
			return
		}

		// Acknowledge the payment, and redirect back to the intended location.
		memo := "Thank you for being a customer"
		payAck := &payments.PaymentACK{
			Payment: payment,
			Memo:    &memo,
		}
		payAckBytes, err := proto.Marshal(payAck)
		if err != nil {
			log.Error().Msgf("unable to unmarshal payment: %s", err)
			http.Error(w, "internal server error", http.StatusInternalServerError)
			return
		}

		w.Header().Add("Content-Type", "application/bitcoincash-paymentack")
		w.Header().Set("Content-Transfer-Encoding", "binary")
		w.Write(payAckBytes)
	}
}
