package main

import (
	"crypto/tls"
	"database/sql"
	"errors"
	"flag"
	"io/ioutil"
	"net/http"
	"os"
	"time"

	"crypto/x509"
	"encoding/hex"
	"encoding/pem"

	"github.com/go-redis/redis"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/spf13/viper"
	"google.golang.org/grpc"
)

type config struct {
	certFile    string
	chainFile   string
	privkeyFile string
	domainName  string
	grpcURI     string
	xpub        string
}

type server struct {
	debug       bool
	cfg         *config
	db          *sql.DB
	rd          *redis.Client
	bchrpc      *grpc.ClientConn
	cert, chain *x509.Certificate
	privkey     interface{}
	wallet      *Wallet
}

var (
	s = &server{
		cfg: &config{},
	}
)

func main() {
	flag.BoolVar(&s.debug, "D", false, "Enable debug output")
	flag.Parse()

	// zerolog.TimeFieldFormat = zerolog.TimeFormatUnix

	zerolog.SetGlobalLevel(zerolog.InfoLevel)
	if s.debug == true {
		zerolog.SetGlobalLevel(zerolog.DebugLevel)
	}
	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr, TimeFormat: time.RFC3339})
	log.Info().Msg("starting go-bip70 server")

	// parse config file
	viper.SetConfigName("config")          // name of config file (without extension)
	viper.AddConfigPath("$HOME/.go-bip70") // call multiple times to add many search paths
	viper.AddConfigPath(".")               // optionally look for config in the working directory
	err := viper.ReadInConfig()            // Find and read the config file
	if err != nil {                        // Handle errors reading the config file
		log.Panic().Msgf("fatal error config file: %s", err)
	}

	s.cfg.certFile = viper.GetString("certificate")
	s.cfg.chainFile = viper.GetString("certificate-chain")
	s.cfg.privkeyFile = viper.GetString("sign-key")
	s.cfg.domainName = viper.GetString("domain-name")
	s.cfg.grpcURI = viper.GetString("grpc-uri")
	s.cfg.xpub = viper.GetString("xpub")

	// init DB connection
	s.db, err = initMySQL()
	if err != nil {
		log.Fatal().Msgf("failed to setup connection to MariaDB server: %s", err.Error())
	}
	log.Info().Int("open_connections", s.db.Stats().OpenConnections).Msg("connected to MariaDB server")

	// init redis connection
	s.rd, err = initRedis()
	if err != nil {
		log.Fatal().Msgf("failed to setup connection to Redis server: %s", err.Error())
	}
	log.Info().Uint32("idle_conns", s.rd.PoolStats().IdleConns).Uint32("total_conns", s.rd.PoolStats().TotalConns).Msg("connected to Redis server")

	// init bchd grpc connection
	s.bchrpc, err = initGRPC()
	if err != nil {
		log.Fatal().Msgf("failed to setup BCHD gRPC connection: %s", err.Error())
	}
	log.Info().Str("URI", s.cfg.grpcURI).Str("state", s.bchrpc.GetState().String()).Msg("connected to BCHD gRPC server")

	chainState, err := getBlockchainInfo()
	if err != nil {
		log.Fatal().Msgf("failed to Bitcoin Cash chain state: %s", err.Error())
	}
	bestBlockHeight := chainState.GetBestHeight()
	bestBlockhash := chainState.GetBestBlockHash()
	log.Info().Int32("best-block-height", bestBlockHeight).Str("best-block-hash", hex.EncodeToString(reverse(bestBlockhash))).Msg("fetched Bitcoin Cash chain state")

	// Wallet
	// xpub := "xpub661MyMwAqRbcGmJB1tNJdre7BBgodbqcaduCeGEVfccqaCSqwUvN17j5ZJou9Dtw7kNpMSBNg5RtenjCKGuuARaUiTbAyesFKzabtTVuhnt"
	s.wallet, err = initWallet(s.cfg.xpub)
	if err != nil {
		log.Fatal().Msgf("failed to initialize wallet: %s", err.Error())
	}
	log.Info().Uint32("index", s.wallet.publicIndex).Str("xpub", s.wallet.XPUB.String()).Msgf("initialized wallet")

	// verify X509 stack
	// _, _, _, err = initX506("cert1.pem", "chain1.pem", "privkey1.pem", "local.devzero.be")
	s.cert, s.chain, s.privkey, err = initX506(s.cfg.certFile, s.cfg.chainFile, s.cfg.privkeyFile, s.cfg.domainName)
	if err != nil {
		log.Fatal().Msgf("failed to init x509 certs: %s", err)
	}

	mux := http.NewServeMux()
	mux.HandleFunc("/", paymentRequestHandler)
	mux.HandleFunc("/pr/", paymentRequestHandler)
	mux.HandleFunc("/payment/", paymentHandler)
	tlsCfg := &tls.Config{
		MinVersion:               tls.VersionTLS12,
		CurvePreferences:         []tls.CurveID{tls.CurveP521, tls.CurveP384, tls.CurveP256},
		PreferServerCipherSuites: true,
		CipherSuites: []uint16{
			tls.TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,
			tls.TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA,
			tls.TLS_RSA_WITH_AES_256_GCM_SHA384,
			tls.TLS_RSA_WITH_AES_256_CBC_SHA,
		},
	}
	srv := &http.Server{
		Addr:         ":4443",
		Handler:      mux,
		TLSConfig:    tlsCfg,
		TLSNextProto: make(map[string]func(*http.Server, *tls.Conn, http.Handler), 0),
	}
	// log.Fatal(srv.ListenAndServeTLS("server.crt", "server.key"))

	log.Info().Msg("starting web-server")
	if err := srv.ListenAndServeTLS("fullchain1.pem", "privkey1.pem"); err != nil {
		log.Fatal().Msgf("fatal http server error: %s\n", err.Error())
	}

}

// returns cert, chain, privkey, err
// func initX506(certName string, chainName string, privName string) ([]byte, []byte, *rsa.PrivateKey, error) {
func initX506(certName string, chainName string, privName string, domainName string) (*x509.Certificate, *x509.Certificate, interface{}, error) {
	certPEM, err := ioutil.ReadFile(certName)
	if err != nil {
		return nil, nil, nil, err
	}

	chainPEM, err := ioutil.ReadFile(chainName)
	if err != nil {
		return nil, nil, nil, err
	}

	privPEM, err := ioutil.ReadFile(privName)
	if err != nil {
		return nil, nil, nil, err
	}

	// First, create the set of root certificates. For this example we only
	// have one. It's also possible to omit this in order to use the
	// default root set of the current operating system.
	roots := x509.NewCertPool()
	ok := roots.AppendCertsFromPEM([]byte(certPEM))
	if !ok {
		return nil, nil, nil, errors.New("failed to parse root certificate")
	}

	// decode PEM cert and parse to DER
	certBlock, _ := pem.Decode([]byte(certPEM))
	if certBlock == nil {
		return nil, nil, nil, errors.New("failed to parse certificate PEM")
	}
	cert, err := x509.ParseCertificate(certBlock.Bytes)
	if err != nil {
		return nil, nil, nil, errors.New("failed to parse certificate: " + err.Error())
	}

	// decode PEM certificate chain and parse to DER
	chainBlock, _ := pem.Decode([]byte(chainPEM))
	if chainBlock == nil {
		return nil, nil, nil, errors.New("failed to parse certificate PEM")
	}
	chain, err := x509.ParseCertificate(chainBlock.Bytes)
	if err != nil {
		return nil, nil, nil, errors.New("failed to parse certificate: " + err.Error())
	}

	opts := x509.VerifyOptions{
		DNSName: domainName,
		Roots:   roots,
	}

	if _, err := cert.Verify(opts); err != nil {
		return nil, nil, nil, errors.New("failed to verify certificate: " + err.Error())
	}

	var block *pem.Block
	if block, _ = pem.Decode([]byte(privPEM)); block == nil {
		return nil, nil, nil, errors.New("decode privkey failed, expected pem block")
	}

	privKey, err := x509.ParsePKCS8PrivateKey(block.Bytes)
	if err != nil {
		return nil, nil, nil, errors.New("parse privkey failed")
	}

	return cert, chain, privKey, nil
}
