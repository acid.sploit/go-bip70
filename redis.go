package main

import (
	"github.com/go-redis/redis"
	"github.com/rs/zerolog/log"
	payments "gitlab.com/acid.sploit/go-bip70/payments"
)

func initRedis() (*redis.Client, error) {
	client := redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "", // no password set
		DB:       0,  // use default DB
	})

	pong, err := client.Ping().Result()
	if err != nil {
		log.Debug().Msgf("failed to setup connection to redis server: %s", err.Error())
		return nil, err
	}
	log.Debug().Str("ping", pong).Msg("setup connection to Redis")

	return client, nil
}

// func save

func insertPRRedis(uid string, pr *payments.PaymentRequest) error {

	return nil
}
