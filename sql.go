package main

import (
	"database/sql"

	"github.com/gcash/bchutil/hdkeychain"
	_ "github.com/go-sql-driver/mysql"
	"github.com/rs/zerolog/log"
	payments "gitlab.com/acid.sploit/go-bip70/payments"
)

func initMySQL() (*sql.DB, error) {
	db, err := sql.Open("mysql", "bip70:bip70pass@tcp(127.0.0.1:3306)/gobip70")
	if err != nil {
		log.Debug().Msgf("faild to setup database connection: %s", err.Error())
		return nil, err
	}

	err = db.Ping()
	if err != nil {
		log.Debug().Msgf("failed to connect to database: %s", err.Error())
		return nil, err
	}
	log.Debug().Str("ping", "PONG").Msg("setup connection to MariaDB")
	// stmt, err := db.Prepare("INSERT INTO paymentrequests(uid) VALUES(?)")
	// if err != nil {
	// 	log.Error().Msg(err.Error())
	// }
	// res, err := stmt.Exec("ab12cd34ef56")
	// if err != nil {
	// 	log.Error().Msg(err.Error())
	// }
	// lastID, err := res.LastInsertId()
	// if err != nil {
	// 	log.Error().Msg(err.Error())
	// }
	// rowCnt, err := res.RowsAffected()
	// if err != nil {
	// 	log.Error().Msg(err.Error())
	// }
	// log.Printf("ID = %d, affected = %d\n", lastID, rowCnt)

	return db, nil
}

func saveWallet(wallet *Wallet) error {
	tx, err := s.db.Begin()
	if err != nil {
		log.Panic().Msg(err.Error())
		return err
	}

	insertWallet, err := tx.Prepare("INSERT INTO wallet VALUES (?,?,?,?,?,?)")
	if err != nil {
		log.Panic().Msg(err.Error())
		return err
	}
	defer insertWallet.Close() // danger!

	_, err = insertWallet.Exec(nil, wallet.XPUB.String(), wallet.publicChain, wallet.changeChain, wallet.publicIndex, wallet.changIndex)
	if err != nil {
		// log.Panic().Msg(err.Error())
		return err
	}

	err = tx.Commit()
	if err != nil {
		// log.Panic().Msg(err.Error())
		return err
	}

	return nil

}

func getWallet(key string) (*Wallet, error) {
	var wallet = Wallet{}
	var id int
	var xpub string

	rows, err := s.db.Query("select * from wallet where xpub like ?", key)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		if err := rows.Scan(&id, &xpub, &wallet.publicChain, &wallet.changeChain, &wallet.publicIndex, &wallet.changIndex); err != nil {
			return nil, err
		}
	}

	wallet.XPUB, err = hdkeychain.NewKeyFromString(xpub)
	if err != nil {
		return nil, err
	}

	return &wallet, nil
}

func hasWallet(key string) bool {
	var id int
	rows, err := s.db.Query("select id from wallet where xpub like ?", key)
	if err != nil {
		return false
	}
	defer rows.Close()

	for rows.Next() {
		if err := rows.Scan(&id); err != nil {
			return false
		}
	}

	return true
}

func updateWallet(wallet *Wallet) error {
	tx, err := s.db.Begin()
	if err != nil {
		log.Panic().Msg(err.Error())
		return err
	}

	updateWallet, err := tx.Prepare("UPDATE wallet SET publicindex = ?, changeindex = ? WHERE xpub like ?")
	if err != nil {
		log.Panic().Msg(err.Error())
		return err
	}
	defer updateWallet.Close() // danger!

	_, err = updateWallet.Exec(wallet.publicIndex, wallet.changIndex, wallet.XPUB.String())
	if err != nil {
		// log.Panic().Msg(err.Error())
		return err
	}

	err = tx.Commit()
	if err != nil {
		// log.Panic().Msg(err.Error())
		return err
	}

	return nil
}

func savePaymentRequest(uid string, pr *payments.PaymentRequest, pd *payments.PaymentDetails, outputs []*payments.Output) error {
	tx, err := s.db.Begin()
	if err != nil {
		log.Panic().Msg(err.Error())
		return err
	}
	defer tx.Rollback()
	insertPR, err := tx.Prepare("INSERT INTO paymentrequests VALUES (?,?)")
	if err != nil {
		log.Panic().Msg(err.Error())
		return err
	}
	defer insertPR.Close() // danger!
	prResult, err := insertPR.Exec(nil, uid)
	if err != nil {
		log.Panic().Msg(err.Error())
		return err
	}

	prid, _ := prResult.LastInsertId()
	insertPD, err := tx.Prepare("INSERT INTO paymentdetails VALUES (?, ?, ?, ?, ?, ?)")
	if err != nil {
		log.Panic().Msg(err.Error())
		return err
	}
	defer insertPD.Close() // danger!
	pdResult, err := insertPD.Exec(nil, prid, pd.GetTime(), pd.GetExpires(), pd.GetMemo(), pd.GetPaymentUrl())
	if err != nil {
		log.Panic().Msg(err.Error())
		return err
	}
	pdid, _ := pdResult.LastInsertId()

	insertOutput, err := tx.Prepare("INSERT INTO outputs VALUES (?, ?, ?, ?, ?)")
	if err != nil {
		log.Panic().Msg(err.Error())
		return err
	}
	defer insertPD.Close() // danger!

	for _, output := range outputs {
		_, err := insertOutput.Exec(nil, pdid, prid, output.GetAmount(), output.GetScript())
		if err != nil {
			log.Panic().Msg(err.Error())
			return err
		}
	}

	err = tx.Commit()
	if err != nil {
		log.Panic().Msg(err.Error())
		return err
	}

	return nil
}
