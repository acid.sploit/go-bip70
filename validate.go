package main

import (
	"crypto/sha256"
	"encoding/hex"
	"errors"
	"os"
	"time"

	"github.com/gcash/bchd/chaincfg"
	"github.com/gcash/bchd/txscript"

	"github.com/gcash/bchutil"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	payments "gitlab.com/acid.sploit/go-bip70/payments"
)

type op struct {
	script []byte
	value  int64
}

func (o *op) setValue(value int64) {
	o.value = value
}

func validatePayment(id string, submittedPayment *payments.Payment, pr *PaymentRequest) error {
	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr, TimeFormat: time.RFC3339})
	log.Info().Msgf("%s VALIDATING submitted payment", id)

	for _, refund := range submittedPayment.GetRefundTo() {
		script := refund.GetScript()
		_, addrs, _, err := txscript.ExtractPkScriptAddrs(script, &chaincfg.MainNetParams)
		if err != nil {
			log.Error().Msgf("%s could not parse refund address", id)
		}

		for _, addr := range addrs {
			log.Info().Msgf("%s refund to: %s", id, addr.String())
		}

	}

	transactions := submittedPayment.GetTransactions()

	// Check if all requested outputs requested by paymentrequest are satisfied by payment
	wanted := make(map[string]*op)
	for _, output := range pr.Outputs {
		bhash := sha256.Sum256(output.Script)
		hash := hex.EncodeToString(bhash[:])
		wanted[hash] = &op{
			script: output.Script,
			value:  int64(output.Amount),
		}
	}

	for _, txBytes := range transactions {
		tx, err := bchutil.NewTxFromBytes(txBytes)
		if err != nil {
			log.Error().Msgf("%s failed bchutil new tx from bytes: %s", id, err.Error())
			return err
		}

		for _, output := range tx.MsgTx().TxOut {
			bhash := sha256.Sum256(output.PkScript)
			hash := hex.EncodeToString(bhash[:])
			if _, ok := wanted[hash]; ok {
				if output.Value == wanted[hash].value {
					delete(wanted, hash)
				} else if wanted[hash].value-output.Value < 0 {
					log.Error().Msgf("%s transaction %s overpaid", id, tx.Hash().String())
					return err
				} else {
					wanted[hash].setValue(wanted[hash].value - output.Value)
				}
			}
		}
	}

	if len(wanted) > 0 {
		log.Error().Msgf("%s payment does not satisfy payment request", id)
		return errors.New("payment does not satisfy payment request")
	}

	// Check fees and validate unspent outputs
	for _, txBytes := range transactions {
		tx, err := bchutil.NewTxFromBytes(txBytes)
		if err != nil {
			log.Error().Msgf("%s failed bchutil new tx from bytes: %s", id, err.Error())
			return err
		}

		inputAmount := int64(0)
		outputAmount := int64(0)
		txSize := int64(tx.MsgTx().SerializeSize())

		for _, input := range tx.MsgTx().TxIn {
			log.Debug().Msgf("%s grpc getUnspentOutput request %s : %d", id, input.PreviousOutPoint.Hash.String(), input.PreviousOutPoint.Index)
			getUnspentOutputResp, err := getUnspentOutput(input.PreviousOutPoint.Hash.CloneBytes(), input.PreviousOutPoint.Index, true)
			if err != nil {
				return errors.New("error grpc request, " + err.Error())
			}

			// Check coinbase
			log.Debug().Msgf("%s is coinbase: %t", id, getUnspentOutputResp.IsCoinbase)
			if getUnspentOutputResp.IsCoinbase {
				curHeight, err := getBlockchainHeight()
				if err != nil {
					return err
				}
				txHeight := getUnspentOutputResp.GetBlockHeight()

				if curHeight-txHeight < 100 {
					return errors.New("coinbase tx is less than 100 blocks old")
				}
			}

			inputAmount += getUnspentOutputResp.Value

			log.Debug().Msgf("%s blockheight: %d", id, getUnspentOutputResp.BlockHeight)
			log.Debug().Msgf("%s outpoint: %x : %d", id, reverse(getUnspentOutputResp.Outpoint.Hash), getUnspentOutputResp.Outpoint.Index)
			log.Debug().Msgf("%s pubkeyscript: %x", id, reverse(getUnspentOutputResp.PubkeyScript))

		}

		for _, output := range tx.MsgTx().TxOut {
			outputAmount += output.Value
		}

		// Check fee
		fee := float64((inputAmount - outputAmount)) / float64(txSize)
		if fee < 1.00 {
			log.Debug().Msgf("%s insufficient fee %4.2f sats/byte for tx: %s", id, fee, tx.MsgTx().TxHash().String())
			return errors.New("insufficient fee ")
		}

		// Submit transaction to network
		resp, err := submitTransaction(txBytes)
		if err != nil {
			log.Debug().Msgf("%s failed to submit tx to network %s", id, err.Error())
			return errors.New("failed to submit tx to network : " + err.Error())
		}
		hash := reverse(resp.Hash)
		log.Info().Msgf("%s transction %x submitted to network ", id, hash)

	}

	return nil

}

func getBlockchainHeight() (int32, error) {
	info, err := getBlockchainInfo()
	if err != nil {
		return 0, err
	}

	return info.GetBestHeight(), nil
}

func reverse(slice []byte) []byte {
	for i := len(slice)/2 - 1; i >= 0; i-- {
		opp := len(slice) - 1 - i
		slice[i], slice[opp] = slice[opp], slice[i]
	}

	return slice
}
