package main

import (
	"github.com/gcash/bchd/chaincfg"
	"github.com/gcash/bchutil"
	"github.com/gcash/bchutil/hdkeychain"
)

// Wallet holds all wallet information to run the bip70 server with a
// new address for each payment according to bip44 spec
type Wallet struct {
	XPRIV       *hdkeychain.ExtendedKey
	XPUB        *hdkeychain.ExtendedKey
	publicChain uint32
	changeChain uint32
	publicIndex uint32
	changIndex  uint32
}

func initWallet(key string) (*Wallet, error) {
	wallet, err := getWallet(key)
	if err == nil && wallet != nil {
		return wallet, nil
	}

	xkey, err := hdkeychain.NewKeyFromString(key)
	if err != nil {
		return nil, err
	}

	if xkey.IsPrivate() {
		xpub, err := xkey.Neuter()
		if err != nil {
			return nil, err
		}
		var wallet = Wallet{
			XPRIV:       xkey,
			XPUB:        xpub,
			publicChain: uint32(0),
			changeChain: uint32(1),
			publicIndex: uint32(0),
			changIndex:  uint32(0),
		}

		return &wallet, nil

	} else {
		var wallet = Wallet{
			XPUB:        xkey,
			publicChain: uint32(0),
			changeChain: uint32(1),
			publicIndex: uint32(0),
			changIndex:  uint32(0),
		}

		return &wallet, nil
	}
}

func (w *Wallet) getNewAddress() (*bchutil.AddressPubKeyHash, error) {
	chainKey, err := w.XPUB.Child(w.publicChain)
	if err != nil {
		return nil, err
	}

	addrKey, err := chainKey.Child(w.publicIndex)
	if err != nil {
		return nil, err
	}

	addr, err := addrKey.Address(&chaincfg.MainNetParams)
	if err != nil {
		return nil, err
	}
	w.publicIndex++

	if err = w.save(); err != nil {
		return nil, err
	}

	return addr, nil
}

func (w *Wallet) save() error {
	if hasWallet(w.XPUB.String()) {
		if err := updateWallet(w); err != nil {
			return err
		}
	} else {
		if err := saveWallet(w); err != nil {
			return err
		}
	}

	return nil
}
